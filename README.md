<style type="text/css">.mandatory-marker { color: red; }
  .warning-message { color: red; margin-top: 8px; margin-bottom: 8px; }
  .invalid-date { width: 100%; margin-top: .25rem; font-size: 80%; color: #dc3545; }
  .custom-file-input:disabled + label { background-color: #e9ecef; opacity: 1; }
  .form-check-label { display: block; margin-left: 28px; }
  input[type="text"], input[type=date]{ height:30px; }
</style>
<link crossorigin="anonymous" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" rel="stylesheet">
<title></title>
<form action="https://cam.maxanywhere.com/WebForms/WebForm" class="needs-validation" data-maxallowedcontentlength="10485760" id="ca919145-e212-4c20-89f1-b7c4afc24f48" method="post" novalidate=""><input name="WebForm_Id" type="hidden" value="ca919145-e212-4c20-89f1-b7c4afc24f48"> <input data-submission-date="" name="feee2a65-565e-4071-beb8-14c6b7963ed5" ref="SIF rec'd" type="hidden"> <input data-submission-date="" name="ede403d4-d404-4cb1-ae27-90f8062497e5" ref="Date uploaded" type="hidden"> <input data-doc-submission-date="" name="4acb137c-60a6-4207-b293-caba8f6cf7fd" ref="SIF Supporting Evidence" type="hidden"> <input id="recaptchaToken" name="recaptchaToken" type="hidden">
<div class="warning-message" id="JavascriptWarning">Javascript must be enabled to be able to submit this form.</div>

<div class="form-group row">
<div class="col-sm-12"><span class="font-weight-bold">Student Information Form</span><br>
The information you provide on this form helps the Accessibility &amp; Accessibility and Disability Resource Centre (ADRC) to work with you and your College and Department/Faculty to arrange your academic-related access requirements. Please complete as much of this form as possible and return it to the ADRC. Completing this form, and sending in supporting documentation will help us in <span class="font-weight-bold">starting the process of arranging your academic-related access requirements.</span><br>
This form is available in alternative formats on request, including Braille. Guidance notes and an electronic version of this form are available at Information for <a href="https://www.disability.admin.cam.ac.uk/information-incoming-students">Incoming Students | Accessibility and Disability Resource Centre (cam.ac.uk)</a><br>
&nbsp;</div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">Section A: Your personal details</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="35007e31-fb06-49f4-ba08-2f98be99b242">First name</label>

<div class="col-sm-10"><input class="form-control" maxlength="79" name="35007e31-fb06-49f4-ba08-2f98be99b242" ref="First name" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="a4d302e7-59c3-4e04-a881-47954b12ec12">Preferred Name</label>

<div class="col-sm-10"><input class="form-control" maxlength="255" name="a4d302e7-59c3-4e04-a881-47954b12ec12" ref="Preferred Name" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="490ac9d7-5eef-4a17-a667-80627624916d">Surname <span class="mandatory-marker">*</span></label>

<div class="col-sm-10"><input class="form-control" maxlength="79" name="490ac9d7-5eef-4a17-a667-80627624916d" ref="Surname" required="" type="text">
<div class="invalid-feedback">Please enter a value for Last name.</div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="ae2efede-a72c-4e3e-a2ac-31cc91c6c629">Preferred Pronouns</label>

<div class="col-sm-10"><input class="form-control" maxlength="255" name="ae2efede-a72c-4e3e-a2ac-31cc91c6c629" ref="Preferred Pronouns" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="4f5e25cb-63a1-42ff-a82e-8b59172e7fc5">Date of Birth <span class="mandatory-marker">*</span></label>

<div class="col-sm-10"><input data-type="date" name="4f5e25cb-63a1-42ff-a82e-8b59172e7fc5" ref="Date of Birth" required="" type="hidden">
<div class="form-group row">
<div class="col" style="max-width: 6em;"><select class="form-control" id="4f5e25cb-63a1-42ff-a82e-8b59172e7fc5_d" onchange="CheckSelectedDate('4f5e25cb-63a1-42ff-a82e-8b59172e7fc5');" ref="Date of birth (day)" required=""><option disabled="disabled" hidden="" selected="selected" value="">dd</option></select></div>

<div class="col" style="max-width: 10em"><select class="form-control" id="4f5e25cb-63a1-42ff-a82e-8b59172e7fc5_m" onchange="CheckSelectedDate('4f5e25cb-63a1-42ff-a82e-8b59172e7fc5');" ref="Date of birth (month)" required=""><option disabled="disabled" hidden="" selected="selected" value="">mm</option></select></div>

<div class="col" style="max-width: 8em;"><select class="form-control" id="4f5e25cb-63a1-42ff-a82e-8b59172e7fc5_y" onchange="CheckSelectedDate('4f5e25cb-63a1-42ff-a82e-8b59172e7fc5');" ref="Date of birth (year)" required=""><option disabled="disabled" hidden="" selected="selected" value="">yyyy</option></select></div>
</div>

<div class="invalid-date" style="display: none;">Please select a valid date for Date of Birth.</div>

<div class="invalid-feedback">Please select a value for Date of Birth.</div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="eb6607ed-0b58-4ec7-b30f-01b1524ab022">E-mail Address <span class="mandatory-marker">*</span></label>

<div class="col-sm-10"><input class="form-control" maxlength="255" name="eb6607ed-0b58-4ec7-b30f-01b1524ab022" ref="E-mail Address" required="" type="text">
<div class="invalid-feedback">Please enter a value for E-mail Address.</div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="5d1f9bd1-4f12-4116-ac1b-1cf3e1b78d9f">UCAS No.</label>

<div class="col-sm-10"><input class="form-control" maxlength="11" name="5d1f9bd1-4f12-4116-ac1b-1cf3e1b78d9f" ref="UCAS Personal ID" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="1cac1a8f-4397-4bc8-8614-0f3783a2acd2">USN (Post-Grads only)</label>

<div class="col-sm-10"><input class="form-control" maxlength="29" name="1cac1a8f-4397-4bc8-8614-0f3783a2acd2" ref="USN CamSIS" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="f06f41e4-a99b-4043-a58b-4cd26c96d466">CRSID</label>

<div class="col-sm-10"><input class="form-control" maxlength="29" name="f06f41e4-a99b-4043-a58b-4cd26c96d466" ref="CRSID" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="8e1edbbd-2a19-4885-8c01-69fa8cb419dc">Course</label>

<div class="col-sm-10"><input class="form-control" maxlength="255" name="8e1edbbd-2a19-4885-8c01-69fa8cb419dc" ref="Course" type="text"></div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="50509446-a651-4ea6-bef4-a1a38fa10770">Month/Year of entry <span class="mandatory-marker">*</span></label>

<div class="col-sm-10"><select class="form-control" name="50509446-a651-4ea6-bef4-a1a38fa10770" ref="Month/Year of entry" required=""><option value=""></option><option value="Apr 2024|">Apr 2024</option><option value="Jan 2024|">Jan 2024</option><option value="Sep/Oct 2023|">Sep/Oct 2023</option><option value="Apr 2023|">Apr 2023</option><option value="Jan 2023|">Jan 2023</option><option value="Sep/Oct 2022|">Sep/Oct 2022</option><option value="2021|">2021</option><option value="2020|">2020</option><option value="2019|">2019</option><option value="2018|">2018</option><option value="2017|">2017</option><option value="2016|">2016</option><option value="2015|">2015</option><option value="2014|">2014</option><option value="2013|">2013</option><option value="2012|">2012</option><option value="2011|">2011</option><option value="2010|">2010</option><option value="2009|">2009</option><option value="2008|">2008</option><option value="2007|">2007</option><option value="2006|">2006</option><option value="2005|">2005</option><option value="2004|">2004</option><option value="2003|">2003</option><option value="2002|">2002</option><option value="2001|">2001</option><option value="2000|">2000</option> </select>

<div class="invalid-feedback">Please select a value for Month/Year of entry.</div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label" for="5d267a5d-a4f1-4a16-a434-6d0983288e0a">College</label>

<div class="col-sm-10"><input class="form-control" maxlength="255" name="5d267a5d-a4f1-4a16-a434-6d0983288e0a" ref="College" type="text"></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">A1. Nature of your disability, impairment or medical condition (please tick all those that apply)</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_1" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have a social/communication impairment such as an autism spectrum condition|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_1">You have a social/communication impairment such as an autism spectrum condition</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_2" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You are blind or have a visual impairment|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_2">You are blind or have a visual impairment</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_3" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You are deaf or have a hearing impairment|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_3">You are deaf or have a hearing impairment</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_4" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have a long standing illness or health condition |"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_4">You have a long standing illness or health condition</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_5" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have a mental health condition, such as depression or anxiety disorder|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_5">You have a mental health condition, such as depression or anxiety disorder</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_6" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have a specific learning difference such as dyslexia, dyspraxia or AD(H)D|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_6">You have a specific learning difference such as dyslexia, dyspraxia or AD(H)D</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_7" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have physical impairment or mobility issues|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_7">You have physical impairment or mobility issues</label></div>

<div class="form-check"><input class="form-check-input" id="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_8" name="c1ae9d89-b555-4b45-93fd-47ce0b5c909a" ref="A1. Nature of your disability, impairment or medical condition " type="checkbox" value="You have a disability, impairment or medical condition that is not listed above|"><label class="form-check-label" for="c1ae9d89-b555-4b45-93fd-47ce0b5c909a_8">You have a disability, impairment or medical condition that is not listed above</label></div>
</div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">Section B: Confidentiality Agreement</div>
</div>

<div class="form-group row">
<div class="col-sm-12"><span class="font-weight-bold">Once you have read the </span><a href="https://www.disability.admin.cam.ac.uk/staff-supporting-disabled-students/confidentiality-0" target="_blank">ADRC Confidentiality Policy</a>, please complete and sign the following Confidentiality Agreement.<br>
<br>
<a href="https://www.disability.admin.cam.ac.uk/staff-supporting-disabled-students/confidentiality-0" target="_blank">Confidentiality | Accessibility and Disability Resource Centre (cam.ac.uk)</a><br>
<br>
Choose one option only:</div>
</div>

<div class="form-group row">
<div class="col-sm-12"><select class="form-control" name="7712773b-a709-4222-a8bd-bb5bccc93d11" ref="Section B: Confidentiality Agreement"><option value=""></option><option value="Full Disclosure">Full Disclosure</option><option value="Restricted disclosure">Restricted disclosure</option> </select></div>
</div>

<div id="disclosureSelections">
<div class="form-group row">
<div class="col-sm-12">If you have selected a restricted disclosure, to which identified groups (choose one option only):</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="form-check"><input class="form-check-input" id="d4a04652-16eb-4b71-a2c2-6c3f3e019008_1" name="d4a04652-16eb-4b71-a2c2-6c3f3e019008" ref="Disclosure to identified groups: " type="radio" value="ADRC Only|"><label class="form-check-label" for="d4a04652-16eb-4b71-a2c2-6c3f3e019008_1">ADRC Only</label></div>

<div class="form-check"><input class="form-check-input" id="d4a04652-16eb-4b71-a2c2-6c3f3e019008_2" name="d4a04652-16eb-4b71-a2c2-6c3f3e019008" ref="Disclosure to identified groups: " type="radio" value="College|"><label class="form-check-label" for="d4a04652-16eb-4b71-a2c2-6c3f3e019008_2">College</label></div>

<div class="form-check"><input class="form-check-input" id="d4a04652-16eb-4b71-a2c2-6c3f3e019008_3" name="d4a04652-16eb-4b71-a2c2-6c3f3e019008" ref="Disclosure to identified groups: " type="radio" value="University|"><label class="form-check-label" for="d4a04652-16eb-4b71-a2c2-6c3f3e019008_3">University</label></div>
</div>
</div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">
<p>B1: Recording of lectures, seminars and supervisions agreement</p>
</div>

<div class="col-sm-12">
<p>In July 2022 the General Board’s Education Committee issued a <a href="https://www.educationalpolicy.admin.cam.ac.uk/supporting-students/policy-recordings/expectation-record-lectures-2022-23" target="_blank">Statement of Expectation for lecture capture recording</a>. This Statement outlines an institutional aim to provide recordings for all lectures as standard. Therefore, in most cases recordings of lectures should be available to students within Moodle. However, there may be some circumstances where students may still need to make recordings.</p>

<p>Please find details of <a href="https://www.disability.admin.cam.ac.uk/students/current-students/advice-and-guidance/recording-lectures" target="_blank">the agreement for the recording of lectures, seminars and supervisions here</a>. We advise that you read the provisions of the agreement and indicate below that you will follow them. We will keep a record of your agreement.</p>
</div>

<div class="col-sm-10">
<div class="form-check"><input class="form-check-input" id="41aa86a8-505e-4619-8135-24e4ce5ff3aa_1" name="41aa86a8-505e-4619-8135-24e4ce5ff3aa" ref="B1: Recording of lectures, seminars and supervisions agreement" type="checkbox" value="Agree|"><label class="form-check-label" for="41aa86a8-505e-4619-8135-24e4ce5ff3aa_1">I have read the provisions of the agreement for the recording of lectures, seminars and supervisions and agree to follow them.</label></div>
</div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">Section C: Your academic-related disability access requirements</div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C1: Please tell us more about your disability/impairment and how it affects your studies.</div>
</div>

<div class="form-group row">
<div class="col-sm-12">Please also attach relevant supporting documentation to this form (e.g. Educational Psychologists assessment, audiology report, or medical letter/report), where available - file uploads can be done at the bottom of the form in section D.<textarea class="form-control" name="0a9102fe-6e63-4479-b467-eecae03cbc24" ref="C1. "></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C2. What support did you receive at school, sixth form, college or other further education or higher education institution, including examination access arrangements?<textarea class="form-control" name="04bc2af7-43c8-4708-bbde-30992d77526c" ref="C2"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C3. Lectures, seminars and supervisions<br>
Please describe any support or access requirements you would need in lectures, seminars and supervisions (tutorials). Examples of support can be found on the ADRC website <a href="https://www.disability.admin.cam.ac.uk/adjustments" target="_blank">click here to view.</a><textarea class="form-control" name="fa8e62fb-0c54-4bed-8fba-05fc54a65e49" ref="C3"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C4. Human Support / Non-Medical Help (NMH)</div>

<div class="col-sm-12">The ADRC will contact you to discuss access requirements, including possible human support. If you have had support previously or feel that this would be useful for you, please describe that support and the reason for that support below.<br>
<span class="font-weight-bold">More information is available online at: </span> <a href="https://www.disability.admin.cam.ac.uk/students/current-students/advice-and-guidance/human-support" target="_blank">Non-Medical Help (NMH) - Human Support | Accessibility and Disability Resource Centre (cam.ac.uk)</a><br>
<textarea class="form-control" name="8c29f3d1-9e6c-4534-b6d2-6838e897aaa0" ref="C4"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C5. Libraries<br>
Are there any adjustments or requirements you would find useful in order to access and use libraries? Examples can be found here on the ADRC website <a href="https://www.disability.admin.cam.ac.uk/adjustments" target="_blank">click here to view.</a><textarea class="form-control" name="cb09a7f4-8e99-4ee5-8954-b6ab8e9b4539" ref="C5"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C6. Practicals, field trips and placements<br>
Please detail any adjustments or access requirements which you may need in practicals (for example in laboratories, fieldtrips or placements<a href="https://www.disability.admin.cam.ac.uk/adjustments" target="_blank"> click here</a>.<textarea class="form-control" name="b7026b5e-2195-49fd-b85a-7c3c7763bd35" ref="C6"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C7. Examination Access Arrangements (EAAs)<br>
Please note that specialist evidence is normally required for requests for examination access arrangements.</div>

<div class="col-sm-12">Your College Tutorial Office will apply for any access arrangements for examinations you require, usually based either on medical evidence or on a full diagnostic report written by a Practitioner Psychologist or Specialist Teacher. The ADRC does not apply on your behalf but we can make recommendations.<br>
<br>
You will need to provide your evidence to your College and ask them to apply on your behalf. Please be aware that deadlines exist for the submission of this evidence. We advise that you provide this in the first term of the academic year.<br>
<br>
Please see the ADRC website for further details and important information about the application process for exam access arrangements: <a href="https://www.disability.admin.cam.ac.uk/current-students/exam-access-arrangements" target="_blank"> Exam Access Arrangements | Accessibility and Disability Resource Centre (cam.ac.uk)</a>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_1" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Extra Time|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_1">Extra Time</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_2" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Use of a word processor|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_2">Use of a word processor</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_3" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Examination paper in alternative format|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_3">Examination paper in alternative format</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_4" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Loop / infrared system for hearing aid|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_4">Loop / infrared system for hearing aid</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_5" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Examinations in a separate room with one-to-one invigilation|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_5">Examinations in a separate room with one-to-one invigilation</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_6" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Examinations in a smaller room with low student numbers|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_6">Examinations in a smaller room with low student numbers</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_7" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Specialist ergonomic seating|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_7">Specialist ergonomic seating</label></div>

<div class="form-check"><input class="form-check-input" id="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_8" name="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1" ref="C7. Examination Access Arrangements (EAAs)" type="checkbox" value="Rest or nutrition breaks|"><label class="form-check-label" for="3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1_8">Rest or nutrition breaks</label></div>
</div>
</div>

<div class="form-group row">
<div class="col-sm-12">Please provide any additional information here:<textarea class="form-control" name="91beda95-7d5b-46d9-984b-53883d94e66f" ref="C7"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">C8 Do you require any of the following?</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_1" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Wheelchair access|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_1">Wheelchair access</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_2" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Level access|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_2">Level access</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_3" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Specialist ergonomic seating|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_3">Specialist ergonomic seating</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_4" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="En-suite accommodation|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_4">En-suite accommodation</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_5" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Accommodation with adapted bathroom|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_5">Accommodation with adapted bathroom</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_6" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Ground floor accommodation|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_6">Ground floor accommodation</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_7" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Vibrating / visual fire alarm|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_7">Vibrating / visual fire alarm</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_8" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="An additional room for your carer or personal assistant|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_8">An additional room for your carer or personal assistant</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_9" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Accommodation suitable for an assistance dog|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_9">Accommodation suitable for an assistance dog</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_10" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Personal Emergency Evacuation Plan|"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_10">Personal Emergency Evacuation Plan</label></div>

<div class="form-check"><input class="form-check-input" id="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_11" name="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7" ref="C8 Do you require any of the following?" type="checkbox" value="Other accommodation requirements, e.g. quiet room/good light/separate fridge. |"><label class="form-check-label" for="e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7_11">Other accommodation requirements, e.g. quiet room/good light/separate fridge.</label></div>
</div>
</div>

<div class="form-group row">
<div class="col-sm-12">Please add a brief description here:<textarea class="form-control" name="07760be9-e426-43fc-b6af-48efc76742c1" ref="C8"></textarea></div>
</div>

<div class="form-group row">
<div class="col-sm-12 font-weight-bold">Section D: Your disability-related funding</div>
</div>

<div class="form-group row">
<div class="col-sm-12">Disabled Students Allowances (DSAs) are available to support UK disabled students, for other specialist human support, travel and equipment/software. Further information on eligibility and application is available here <a href="https://www.gov.uk/disabled-students-allowance-dsa" target="_blank">Help if you're a student with a learning difficulty, health problem or disability: Disabled Students' Allowance - GOV.UK (www.gov.uk)</a><br>
<br>
The ADRC also has internal funds which may be able to support you. Your adviser will discuss this with you. More information on funding is available on the ADRC website:<a href="https://www.disability.admin.cam.ac.uk/funding-your-support-0" target="_blank"> Funding your support | Accessibility and Disability Resource Centre (cam.ac.uk)</a><br>
<br>
If you have any questions related to this please email <a href="disability@admin.cam.ac.uk" target="_blank"> disability@admin.cam.ac.uk</a> or phone 01223 332301</div>
</div>

<div class="form-group row">
<div class="col-sm-12">1.Supporting Evidence</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="custom-file"><input accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" class="custom-file-input" data-title-control="1MedicalEvidenceType" id="2bfc72ab-55f8-46a2-9629-8e8c5a1e90be" name="2bfc72ab-55f8-46a2-9629-8e8c5a1e90be" ref="1.Medical Evidence" type="file"><label class="custom-file-label" for="2bfc72ab-55f8-46a2-9629-8e8c5a1e90be">Choose file</label><button class="ClearFileButton btn btn-link" onclick="ClearFileInput('2bfc72ab-55f8-46a2-9629-8e8c5a1e90be');" style="display: none;" type="button">Clear selected file</button></div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label">Type</label>

<div class="col-sm-10"><select class="form-control" id="1MedicalEvidenceType"><option value=""></option><option>Consultants Letter/Report</option><option>Diagnostic Test Evidence</option><option>Doctor's Letter/Note</option><option>Hospital Letter</option><option>JCQ Form 8</option><option>Medical Records</option><option>Medical Test Evidence</option><option>Needs Assessment Report</option><option>Occupational Therapists Report</option><option>Physiotherapist Report</option><option>Practitioner Psychologists Report</option><option>Psychiatrists Report/Letter</option><option>School/College Letter</option><option>Speech and Language Therapist's Report</option><option>Specialist Teacher's Report</option><option>Student Finance Letter</option><option>Other</option> </select></div>
</div>

<div class="form-group row">
<div class="col-sm-12">2.Supporting Evidence</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="custom-file"><input accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" class="custom-file-input" data-title-control="2MedicalEvidenceType" id="904c9df4-4694-4ae8-b2f1-4a0318362cc5" name="904c9df4-4694-4ae8-b2f1-4a0318362cc5" ref="2.Medical Evidence" type="file"><label class="custom-file-label" for="904c9df4-4694-4ae8-b2f1-4a0318362cc5">Choose file</label><button class="ClearFileButton btn btn-link" onclick="ClearFileInput('904c9df4-4694-4ae8-b2f1-4a0318362cc5');" style="display: none;" type="button">Clear selected file</button></div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label">Type</label>

<div class="col-sm-10"><select class="form-control" id="2MedicalEvidenceType"><option value=""></option><option>Consultants Letter/Report</option><option>Diagnostic Test Evidence</option><option>Doctor's Letter/Note</option><option>Hospital Letter</option><option>JCQ Form 8</option><option>Medical Records</option><option>Medical Test Evidence</option><option>Needs Assessment Report</option><option>Occupational Therapists Report</option><option>Physiotherapist Report</option><option>Practitioner Psychologists Report</option><option>Psychiatrists Report/Letter</option><option>School/College Letter</option><option>Speech and Language Therapist's Report</option><option>Specialist Teacher's Report</option><option>Student Finance Letter</option><option>Other</option> </select></div>
</div>

<div class="form-group row">
<div class="col-sm-12">3.Supporting Evidence</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="custom-file"><input accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" class="custom-file-input" data-title-control="3MedicalEvidenceType" id="4f98ad4d-b744-4503-8ea8-f774180f7d30" name="4f98ad4d-b744-4503-8ea8-f774180f7d30" ref="3.Medical Evidence" type="file"><label class="custom-file-label" for="4f98ad4d-b744-4503-8ea8-f774180f7d30">Choose file</label><button class="ClearFileButton btn btn-link" onclick="ClearFileInput('4f98ad4d-b744-4503-8ea8-f774180f7d30');" style="display: none;" type="button">Clear selected file</button></div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label">Type</label>

<div class="col-sm-10"><select class="form-control" id="3MedicalEvidenceType"><option value=""></option><option>Consultants Letter/Report</option><option>Diagnostic Test Evidence</option><option>Doctor's Letter/Note</option><option>Hospital Letter</option><option>JCQ Form 8</option><option>Medical Records</option><option>Medical Test Evidence</option><option>Needs Assessment Report</option><option>Occupational Therapists Report</option><option>Physiotherapist Report</option><option>Practitioner Psychologists Report</option><option>Psychiatrists Report/Letter</option><option>School/College Letter</option><option>Speech and Language Therapist's Report</option><option>Specialist Teacher's Report</option><option>Student Finance Letter</option><option>Other</option> </select></div>
</div>

<div class="form-group row">
<div class="col-sm-12">4.Supporting Evidence</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="custom-file"><input accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" class="custom-file-input" data-title-control="4MedicalEvidenceType" id="d8ba27d4-7377-403c-b446-ed87028000a1" name="d8ba27d4-7377-403c-b446-ed87028000a1" ref="4.Medical Evidence" type="file"><label class="custom-file-label" for="d8ba27d4-7377-403c-b446-ed87028000a1">Choose file</label><button class="ClearFileButton btn btn-link" onclick="ClearFileInput('d8ba27d4-7377-403c-b446-ed87028000a1');" style="display: none;" type="button">Clear selected file</button></div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label">Type</label>

<div class="col-sm-10"><select class="form-control" id="4MedicalEvidenceType"><option value=""></option><option>Consultants Letter/Report</option><option>Diagnostic Test Evidence</option><option>Doctor’s Letter/Note</option><option>Hospital Letter</option><option>JCQ Form 8</option><option>Medical Records</option><option>Medical Test Evidence</option><option>Needs Assessment Report</option><option>Occupational Therapists Report</option><option>Physiotherapist Report</option><option>Practitioner Psychologists Report</option><option>Psychiatrists Report/Letter</option><option>School/College Letter</option><option>Speech and Language Therapist's Report</option><option>Specialist Teacher's Report</option><option>Student Finance Letter</option><option>Other</option> </select></div>
</div>

<div class="form-group row">
<div class="col-sm-12">5.Supporting Evidence</div>
</div>

<div class="form-group row">
<div class="col-sm-12">
<div class="custom-file"><input accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" class="custom-file-input" data-title-control="5MedicalEvidenceType" id="8871a3dd-14a4-45ff-9cda-5e74552e17bd" name="8871a3dd-14a4-45ff-9cda-5e74552e17bd" ref="5.Medical Evidence" type="file"><label class="custom-file-label" for="8871a3dd-14a4-45ff-9cda-5e74552e17bd">Choose file</label><button class="ClearFileButton btn btn-link" onclick="ClearFileInput('8871a3dd-14a4-45ff-9cda-5e74552e17bd');" style="display: none;" type="button">Clear selected file</button></div>
</div>
</div>

<div class="form-group row"><label class="col-sm-2 col-form-label">Type</label>

<div class="col-sm-10"><select class="form-control" id="5MedicalEvidenceType"><option value=""></option><option>Consultants Letter/Report</option><option>Diagnostic Test Evidence</option><option>Doctor's Letter/Note</option><option>Hospital Letter</option><option>JCQ Form 8</option><option>Medical Records</option><option>Medical Test Evidence</option><option>Needs Assessment Report</option><option>Occupational Therapists Report</option><option>Physiotherapist Report</option><option>Practitioner Psychologists Report</option><option>Psychiatrists Report/Letter</option><option>School/College Letter</option><option>Speech and Language Therapist's Report</option><option>Specialist Teacher's Report</option><option>Student Finance Letter</option><option>Other</option> </select></div>
</div>

<div class="warning-message" id="MandatoryWarning" style="display: none;">Please ensure all fields marked with a * have a value before submitting this form.</div>

<div class="warning-message" id="ErrorMessage" style="display: none">&nbsp;</div>
<input class="btn btn-primary" disabled="true" id="SubmitButton" type="submit" value="Submit"> <!--required to display progress of file upload-->
<div id="UploadProgressContainer"><progress id="UploadProgressBar" style="display: none; width: 10em;"></progress>

<div id="UploadProgressMessage">&nbsp;</div>
</div>
</form>
<script src="https://www.google.com/recaptcha/api.js?render=6LcXLyAhAAAAADPw_jrD24ox2YP9OCfbq5KeQJW8"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script><script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><script>
  <!--//--><![CDATA[// ><!--

  $('select[name="7712773b-a709-4222-a8bd-bb5bccc93d11"]').on('change', function() {
  if(this.value==="Full Disclosure"){
    $( "#d4a04652-16eb-4b71-a2c2-6c3f3e019008_1" ).prop( "checked", false );
    $( "#d4a04652-16eb-4b71-a2c2-6c3f3e019008_2" ).prop( "checked", false );
    $( "#d4a04652-16eb-4b71-a2c2-6c3f3e019008_3" ).prop( "checked", false );
    $( "#disclosureSelections").hide();
  } else { 
    $( "#disclosureSelections").show();
  }
});

  document.getElementById('JavascriptWarning').style.display = 'none';
  document.getElementById('SubmitButton').disabled = false;


  (function() {
    'use strict';
    window.addEventListener('load', function() {
    
      var form = document.getElementById('ca919145-e212-4c20-89f1-b7c4afc24f48');
      form.addEventListener('submit', function(event) {
        $('.is-invalid').removeClass('is-invalid');
        // prevent automatic form submission
        event.preventDefault();
        event.stopPropagation();
        // if a check box field group is mandatory, determine if any are un-checked and mark them as required
        var requiredCheckboxGroups = form.querySelectorAll('div[required]');
        for (var i = 0; i < requiredCheckboxGroups.length; i++) {
          if (requiredCheckboxGroups[i].querySelectorAll(':checked').length == 0) {
            requiredCheckboxGroups[i].querySelector('input').setAttribute('required', true);
          } else {
            requiredCheckboxGroups[i].querySelector('input').removeAttribute('required');
          }
        }
        CheckForBlankDates();
        if (form.checkValidity() === false) {
          document.getElementById('MandatoryWarning').style.display = '';
          document.getElementById('ErrorMessage').style.display = 'none';
          // highlight all form elements that failed to validate
          var invalidGroup = form.querySelectorAll(':invalid');
          for (var i = 0; i < invalidGroup.length; i++) {
            invalidGroup[i].classList.add('is-invalid');
          }
        }
        else if (CheckDates() == false) {
          document.getElementById('MandatoryWarning').style.display = 'none';
        }
        else {
          document.getElementById('ErrorMessage').style.display = 'none';
          document.getElementById('MandatoryWarning').style.display = 'none';
          var submitButton = document.getElementById('SubmitButton');
          submitButton.setAttribute('data-defaultvalue', submitButton.value);
          submitButton.value = 'Submitting...';
          submitButton.disabled = true;
          // submit the form with jQuery to allow file uploads and/or RECaptcha
          SubmitForm(form);
        }
        
      }, false);

      // prevent form submission when enter key is pressed
      form.addEventListener('keypress', function(event) {
        var key = event.charCode || event.keyCode || 0;
        if (key == 13) {
          event.preventDefault();
        }
      });

      // prevent non-numeric characters from being entered into numeric fields
      var numberFields = document.querySelectorAll('input[data-type="number"]');
      for (var i = 0; i < numberFields.length; i++) {
        numberFields[i].setAttribute('onkeypress', 'return isNumber(event)');
        numberFields[i].setAttribute('onpaste', 'return checkForNonNumbers(event)');
      }

      // update selected file label, check file size and allowed file types for Bootstrap custom file input
      var fileInputs = document.getElementsByClassName('custom-file-input');
      Array.prototype.filter.call(fileInputs, function(fileInput) {
        if (fileInput.files.length > 0) {
         $(fileInput).next('.custom-file-label').html(fileInput.files[0].name);
         $(fileInput).next().next('.ClearFileButton').show();
        }
        fileInput.addEventListener('change', function(event) {
          if (IsValidFileType(this)) {
            if (CheckFileSize(this)) {
              var fileName = event.target.files[0].name;
              $(this).next('.custom-file-label').html(fileName);
              $(this).siblings('.ClearFileButton').first().show();
            }
            else {
              $(this).val();
              alert('File size exceeds the maximum allowed.');
              ClearFileInput(this.id);
            }
          }
          else {
            $(this).val('');
            alert('File type not allowed.');
            ClearFileInput(this.id);
          }
        });
      });
    }, false);
    PopulateDateSelectors();
  })();

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ( (charCode > 31 && charCode < 48) || charCode > 57) {
      if (evt.target.getAttribute('data-allowdecimals') != null && charCode == 46 && evt.target.value.indexOf('.') == -1)
        return true;
      else
        return false;
    }
    return true;
  }

  function checkForNonNumbers(evt) {
    var paste = (evt.clipboardData || window.clipboardData).getData('text');
    evt = (evt) ? evt : window.event;
    evt.target.value = paste.replace(/\D/g, '');
    return false;
  }

  function PopulateDateSelectors() {
    var datepickers = $('input[data-type="date"]');
      for (i = 0; i < datepickers.length; i++) {
        var name = $(datepickers[i]).attr('name');
        var dayPicker = $('#' + name + '_d')[0];
        var monthPicker = $('#' + name + '_m')[0];
        var yearPicker = $('#' + name + '_y')[0];
        for (d = 1; d < 32; d++) {
          var day = pad(d, 2);
          dayPicker.add(new Option(day, day), undefined);
      }
      var monthNames = ['January','February','March','April','May','June','July','August','September','October','November','December'];
      for (m = 0; m < 12; m++) {
        var month = pad((m + 1), 2);
        monthPicker.add(new Option(monthNames[m], month));
      }
      for (y = 2023; y > 1922; y--) {
        var year = y.toString();
        yearPicker.add(new Option(year, year));
      }
    }
  }

  function CheckSelectedDate(name) {
    var dayPicker = $('#' + name + '_d');
    var monthPicker = $('#' + name + '_m');
    var yearPicker = $('#' + name + '_y');
    if (Number(dayPicker.val()) > 0 && Number(monthPicker.val()) > 0 && Number(yearPicker.val()) > 0) {
      var date = yearPicker.val() + '-' + monthPicker.val() + '-' + dayPicker.val();
      var warningMessage = $('input[name="' + name + '"]').first().siblings('.invalid-date').first();
      if (isValidDate(date) === false) {
        warningMessage.show();
        warningMessage.removeClass('is-invalid');
      }
      else {
        warningMessage.hide();
      }
    }
  }
  
  function CheckDates() {
    var datepickers = $('input[data-type="date"]');
    for (i = 0; i < datepickers.length; i++) {
      var datepicker = $(datepickers[i]);
      var name = datepicker.attr('name');
      var date;
      var day = $('#' + name + '_d').val();
      var month = $('#' + name + '_m').val();
      var year = $('#' + name + '_y').val();
      date = year + '-' + month + '-' + day;

      if (isValidDate(date) === false) {
        $('#ErrorMessage').html('"' + datepicker.attr('ref') + '" is not a valid date.');
        $('#ErrorMessage').show();
        return false;
      }
      else {
        datepicker.val(date);
      }

    }
    return true;
  }

  function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }

  function isValidDate(dateStr) {
    var parts = dateStr.split('-');
    var date = new Date(parts[0] + '-' + parts[1] + '-' + parts[2]);
    return !!(date && (date.getMonth() + 1) == parts[1] && date.getDate() == Number(parts[2]));
  }

  function CheckForBlankDates() {
    var datepickers = $('input[data-type="date"][required=""]');
    for (i = 0; i < datepickers.length; i++) {
      var datepicker = $(datepickers[i]);
      var name = datepicker.attr('name');
      var day = $('#' + name + '_d').val();
      var month = $('#' + name + '_m').val();
      var year = $('#' + name + '_y').val();
      var invalidFeedback = $('input[name="' + name + '"]').first().siblings('.invalid-feedback').first();
      if (day == null && month == null && year == null) {
        invalidFeedback.css('display', 'block');
      }
      else {
        invalidFeedback.css('display', 'none');
      }
    }
  }

  function getUTCDateString(date, includeTime) {
	  var utcDate = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    if (includeTime == true)
  	  return date.toISOString();
    else
  	  return date.toISOString().substr(0, 10);
  }

  // functions to handle form submission with jQuery and file uploads
  var currentForm;
  var entityKey;
  var fileIndex;
  var fileCount;
  var fileInputs;
  var webFormId;
  var redirectUrl;
  var sessionKey;
  
  function SubmitForm(form) {
    $('#UploadProgressMessage').hide();
    $('#ErrorMessage').hide();
    
    $('#recaptchaToken').val('');
    grecaptcha.ready(function() {
      grecaptcha.execute('6LcXLyAhAAAAADPw_jrD24ox2YP9OCfbq5KeQJW8', { action: 'submit' }).then(function (token) {
        $('#recaptchaToken').val(token);
        // populate all form submission date fields with today's date in UTC
        var submissionDate = getUTCDateString(new Date());
        var submissionDateFields = $('input[data-submission-date]');
        $(submissionDateFields).val(submissionDate);
        var docSubmissionDateFields = $('input[data-doc-submission-date]');
        // get all file inputs with a file selected
        fileInputs = $(form).find('input[type="file"]').filter(function() { return $(this).val() != ''; });
        if (fileInputs.length > 0) {
          $(docSubmissionDateFields).val(submissionDate);
        }
        else {
          // clear all fields for document submission date when the form is submitted without documents
          $(docSubmissionDateFields).val('');
        }
        
        var formData = $(form).serialize();
        fileInputs = $(form).find('input[type="file"]');
        for (i = 0; i < fileInputs.length; i++) {
          var fileInput = fileInputs[i];
          if (fileInput.files[0] != null) {
            var title = $('#' + $(fileInput).data('title-control')).val();
            if (title == null || (title != null && title == '')) {
              title = fileInput.files[0].name;
            }
            formData += '&' + encodeURIComponent($(fileInput).attr('name')) + '=' + encodeURIComponent(title);
            $(docSubmissionDateFields).val(submissionDate);
          }
        }
        formData += '&ajax=true';
        currentForm = form;
        ToggleFileInputs(true);
        $.ajax({
          url: $(form).attr('action'),
          type: 'POST',
          dataType: 'json',
          data: formData,
          success: function(data) {
            if (data != null) {
              if (data.Code == 0) {
                entityKey = data.Key;
                redirectUrl = data.RedirectUrl;
                sessionKey = data.SessionKey;
                UploadDocuments(form, redirectUrl);
              }
              else {
                ShowErrorMessage('There was a problem processing the web form (error code ' + data.ErrorCode + ').');
              }
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            if (jqXHR.responseJSON) {
              ShowErrorMessage('There was a problem submitting the form (error code ' + jqXHR.responseJSON.ErrorCode + ').');
            }
            else {
              ShowErrorMessage('There was a problem submitting the form (' + textStatus + ').');
            }
            $('#SubmitButton').val($('#SubmitButton').data('defaultvalue'));
            ToggleFileInputs(false);
            window.setTimeout(function() { document.getElementById('SubmitButton').disabled = false; }, 3000);
          }
        });
      });
    });
  }

  function UploadDocuments() {
    webFormId = $(currentForm).find('input[name="WebForm_Id"]').val();    
    var formFileInputs = $(currentForm).find('input[type="file"]');
    fileInputs = [];
    fileCount = 0;
    for (i = 0; i < formFileInputs.length; i++) {
      if (formFileInputs[i].files[0] != null) {
        fileInputs.push(formFileInputs[i]);
      }
    }
    fileCount = fileInputs.length;
    if (fileCount > 0)
    {
      fileIndex = 0;
      UploadDocument();
    }
    else {
      ResetForm(currentForm);
      if (redirectUrl) {
        window.location = redirectUrl;
      }
      else {
        document.write('Thank you for submitting the form, ADRC will contact you in due course.<br>Click <a href="https://www.disability.admin.cam.ac.uk">here to return to our homepage.');
      }
    }
  }

  function UploadDocument() {
    if (fileIndex >= fileCount) {
      ResetForm(currentForm);
      if (redirectUrl) {
        window.location = redirectUrl;
      }
      else {
        document.write('Thank you for submitting the form, ADRC will contact you in due course.<br>Click <a href="https://www.disability.admin.cam.ac.uk">here to return to our homepage.');
      }
      return;
    }
    var fileInput = fileInputs[fileIndex];
    var file = fileInput.files[0];
    var fieldId = $(fileInput).attr('name');
    if (file == null) {
      return;
    }
    var title = $('#' + $(fileInput).data('title-control')).val();
    var formData = new FormData();
    var progressBar = $('#UploadProgressBar');
    var progressContainer = $('#UploadProgressContainer');
    var progressMessage = $('#UploadProgressMessage');
    var errorMessage = $('#ErrorMessage');
    formData.append('WebForm_Id', webFormId);
    formData.append('Field_Id', fieldId);
    formData.append('Parent_Key', entityKey);
    formData.append('Session_Key', sessionKey);
    formData.append('file', file);
    if (title != null && title != '') {
      formData.append('Title', title);
    }
    console.log('Posting file to ' + $(currentForm).attr('action') + '/UploadDocument');
    $.ajax({
      url: $(currentForm).attr('action') + '/UploadDocument',
      type: 'POST',
      dataType: 'json',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(documentKey) {
        console.log('Document ' + (fileIndex + 1) + ' upload complete.');
        ClearFileInput(fileInputs[fileIndex].id, 'File upload complete');
        fileIndex++;
        UploadDocument();
      },
      xhr: function() {
        var fileXhr = $.ajaxSettings.xhr();
        if (fileXhr.upload) {
          progressBar.show();
          progressMessage.show();
          $(progressMessage).html('Uploading document ' + (fileIndex + 1) + ' of ' + fileCount + '...'); 
          fileXhr.upload.addEventListener("progress", function(e) {
            if (e.lengthComputable) {
              progressBar.attr({
                value: e.loaded,
                max: e.total
              });
              if (e.loaded == e.total) {
                console.log('Processing file...');
                $(progressMessage).html('Processing document ' + (fileIndex + 1) + ' of ' + fileCount + '...'); 
              }
            }
          }, false);
        }
        return fileXhr;
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);

        $(progressMessage).append('failed.');

        if (jqXHR.status == 413) {
          console.log('Document exceeds file size limit.');
          ShowErrorMessage('The document "' + file.name + '" exceeds the upload file size limit.');
        }
        else if (jqXHR.status == 403) {
          console.log('Upload denied');
          console.log(responseJSON);
          ShowErrorMessage('There was a problem with the document upload (error code 403).');
        }
        else if (jqXHR.Message = 3000) {
          ShowErrorMessage('The document "' + file.name + '" is not a supported file type.');
        }
        else {
          if (jqXHR.responseJSON) {
            ShowErrorMessage('There was a problem with the document upload (error code ' + jqXHR.responseJSON.ErrorCode + ').');
          }
          else {
            ShowErrorMessage('There was a problem with the document upload.');
          }
        }
        progressBar.hide();
        $('#SubmitButton').val($('#SubmitButton').data('defaultvalue'));
        ToggleFileInputs(false);
        window.setTimeout(function() { document.getElementById('SubmitButton').disabled = false; }, 3000);
      }
    });
  }

  // helper functions
  function ClearFileInput(id, text) {
    if (!text)
      text = 'Choose file';
    var fileInput = $('#' + id);
    $(fileInput).val('');
    $(fileInput).next('.custom-file-label').html(text);
    $(fileInput).siblings('.ClearFileButton').first().hide();
  }

  function ToggleFileInputs(disable)
  {
    var fileInputs = document.getElementsByClassName('custom-file-input');
    if (fileInputs.length == 0) {
      fileInputs = $('input[type="file"]');
    }
    Array.prototype.filter.call(fileInputs, function(fileInput) {
      $(fileInput).prop('disabled', disable);
      if ($(fileInput).hasClass('custom-file-input')) {
        $(fileInput).siblings('.btn').first().prop('disabled', disable);
      }
    });
  }

  function IsValidFileType(fileInput) {
    var accept = $(fileInput).attr('accept').split(',');
    var fileName = fileInput.files[0].name;
    var fileExtension = fileName.substring(fileName.lastIndexOf('.'));
    return accept.includes(fileExtension);
  }

  function CheckFileSize(fileInput) {
    var form = $(fileInput).closest('form');
    var maxFileSize = $(form).data('maxallowedcontentlength') * 1;
    return fileInput.files[0].size <= maxFileSize;
  }

  function ShowErrorMessage(message) {
    var errorMessage = $('#ErrorMessage');
    $(errorMessage).show();
    message += '<br>Please contact ADRC at <a href="mailto:disability@admin.cam.ac.uk">disability@admin.cam.ac.uk';
    $(errorMessage).html(message);
  }

  function ResetForm(form) {
    $(form).trigger('reset');
    Array.prototype.filter.call($(form).find('.custom-file-input'), function(fileInput) {
      ClearFileInput($(fileInput).attr('id'));
    });
  }

  function checkAgreement(){
    
  }

//--><!]]>
</script>

WebForm_Id=ca919145-e212-4c20-89f1-b7c4afc24f48
feee2a65-565e-4071-beb8-14c6b7963ed5=2023-09-21
ede403d4-d404-4cb1-ae27-90f8062497e5=2023-09-21
4acb137c-60a6-4207-b293-caba8f6cf7fd=2023-09-21
recaptchaToken=03AFcWeA6aUHRyNN5A0CpfpnuE8m_vSnQ5b2RcxOCrLmpswA_wpLjdUcvApH92YA-Hg_FOtTnLIXecHVu88GL3kpNpVjJFARd4ljQaE5PScPzhRkquCFFGVf_-O2FM994iw0GxlWzVsc_j6DAu-gpkUPj9a7g9KkWISzDIBFbgYtMHtVuBICaLZ6DlgnpKQ0ECRQiYTGuIYsCPkbyyE87QTY6eLEhJrNFQ5HKXTULihloBHCnqBodOHKFdTBuZDSkXWPG-DQu8OXL4DUdZ6e8cDsUnodzHKhqX7_QwkxfheJmYW3r0oTGBhniTybm28seLmlZfs9TBfIhq4_bsYxTf8Veg0Z7-7yuwuZekxodvufAzot8yLMCv2yfMAg1NWXTlYAg0NUNZwi7Ta-nsC2FI17lvyVH7N3AKyeL861ujXOZDh4blz-nMtLrKEqfc_h-mJ__1hjAOJEv8bZz3vC8rneeBCH86TGceA7GUg3wPll1O233jExM_Mle0ll7ceP65_7rKv1B2mbEf0pL9vOmT7UJ0uJLzRv81Q_eRlKyYxTmGFwk5HJMc3n0
35007e31-fb06-49f4-ba08-2f98be99b242=OLDLIVETEST+(firstname)
a4d302e7-59c3-4e04-a881-47954b12ec12=OLDLIVETEST+(preferred)
490ac9d7-5eef-4a17-a667-80627624916d=OLDLIVETEST+(lastname)
ae2efede-a72c-4e3e-a2ac-31cc91c6c629=OLDLIVETEST+(pronouns)
4f5e25cb-63a1-42ff-a82e-8b59172e7fc5=2005-01-20
eb6607ed-0b58-4ec7-b30f-01b1524ab022=test%40olamalu.com
5d1f9bd1-4f12-4116-ac1b-1cf3e1b78d9f=1234569
1cac1a8f-4397-4bc8-8614-0f3783a2acd2=OLDLIVETEST+(urn)
f06f41e4-a99b-4043-a58b-4cd26c96d466=OLDLIVETEST+(crs)
8e1edbbd-2a19-4885-8c01-69fa8cb419dc=OLDLIVETEST+(course)
50509446-a651-4ea6-bef4-a1a38fa10770=Apr+2024%7C
5d267a5d-a4f1-4a16-a434-6d0983288e0a=OLDLIVETEST+(college)
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+a+social%2Fcommunication+impairment+such+as+an+autism+spectrum+condition%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+are+blind+or+have+a+visual+impairment%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+are+deaf+or+have+a+hearing+impairment%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+a+long+standing+illness+or+health+condition+%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+a+mental+health+condition%2C+such+as+depression+or+anxiety+disorder%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+a+specific+learning+difference+such+as+dyslexia%2C+dyspraxia+or+AD(H)D%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+physical+impairment+or+mobility+issues%7C
c1ae9d89-b555-4b45-93fd-47ce0b5c909a=You+have+a+disability%2C+impairment+or+medical+condition+that+is+not+listed+above%7C
7712773b-a709-4222-a8bd-bb5bccc93d11=Restricted+disclosure
d4a04652-16eb-4b71-a2c2-6c3f3e019008=ADRC+Only%7C
41aa86a8-505e-4619-8135-24e4ce5ff3aa=Agree%7C
0a9102fe-6e63-4479-b467-eecae03cbc24=OLDLIVETEST+(c1)
04bc2af7-43c8-4708-bbde-30992d77526c=OLDLIVETEST+(c2)
fa8e62fb-0c54-4bed-8fba-05fc54a65e49=OLDLIVETEST+(c3)
8c29f3d1-9e6c-4534-b6d2-6838e897aaa0=OLDLIVETEST+(c4)
cb09a7f4-8e99-4ee5-8954-b6ab8e9b4539=OLDLIVETEST+(c5)
b7026b5e-2195-49fd-b85a-7c3c7763bd35=OLDLIVETEST+(c6)
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Extra+Time%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Use+of+a+word+processor%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Examination+paper+in+alternative+format%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Loop+%2F+infrared+system+for+hearing+aid%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Examinations+in+a+separate+room+with+one-to-one+invigilation%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Examinations+in+a+smaller+room+with+low+student+numbers%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Specialist+ergonomic+seating%7C
3de9902c-6a8c-4fe2-b6eb-f8d1feac8de1=Rest+or+nutrition+breaks%7C
91beda95-7d5b-46d9-984b-53883d94e66f=Exams+(other)
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Wheelchair+access%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Level+access%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Specialist+ergonomic+seating%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=En-suite+accommodation%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Accommodation+with+adapted+bathroom%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Ground+floor+accommodation%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Vibrating+%2F+visual+fire+alarm%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=An+additional+room+for+your+carer+or+personal+assistant%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Accommodation+suitable+for+an+assistance+dog%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Personal+Emergency+Evacuation+Plan%7C
e0215bc0-6ad8-4458-b4c5-b247fa8c5ba7=Other+accommodation+requirements%2C+e.g.+quiet+room%2Fgood+light%2Fseparate+fridge.+%7C
07760be9-e426-43fc-b6af-48efc76742c1=College+(other)
2bfc72ab-55f8-46a2-9629-8e8c5a1e90be=Student+Finance+Letter
ajax=true

returns 

{
  "Code": 0,
  "Key": "SW5kaXZpZHVhbAkyMzA5MTgyNTE5NDY0NzQxNzAwMDJDCTA=",
  "SessionKey": "M/hklxrV8ijxGjrXsz2dyGk3wP29n62WBc8CKvnW3jgyXaIBUCXAbdBfPyKsEShNxOVi5T2R4KpVrfeX3g=="
}


POST
	https://cam.maxanywhere.com/WebForms/WebForm/UploadDocument

  ----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="WebForm_Id"

ca919145-e212-4c20-89f1-b7c4afc24f48
-----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="Field_Id"

2bfc72ab-55f8-46a2-9629-8e8c5a1e90be
-----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="Parent_Key"

SW5kaXZpZHVhbAkyMzA5MTgyNTE5NDY0NzQxNzAwMDJDCTA=
-----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="Session_Key"

M/hklxrV8ijxGjrXsz2dyGk3wP29n62WBc8CKvnW3jgyXaIBUCXAbdBfPyKsEShNxOVi5T2R4KpVrfeX3g==
-----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="file"; filename="test-image.png"
Content-Type: image/png

PNG

   
IHDR  Â      °bå   sBIT|d   	pHYs  Ä  Ä+   tEXtSoftware www.inkscape.orgî<  nIDATxíØAJaáóÛ9rj6Ð¼©[p[í E® IP¢¤AÀB£è"ôñs_×
Ñ{Ã;Ý<nlåÓéÞï¯'»×£w -Gí¿úðâ`ô¹Û=  FB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò´åùÝëÑ#æîêùÎfô çêÉÍùË7|KÓ»³G?G Q¼FHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MH[NooF½×«¿wÇËÍè@Ëå÷½Õ/×£wÌÝrzÿoô¹»{úàvq¼Bà¿úu¹^}ûxèoÉk4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MHB Ò4! MH»¦U,ä¨\+    IEND®B`
-----------------------------59745580229283319161391300394
Content-Disposition: form-data; name="Title"

Hospital Letter
-----------------------------59745580229283319161391300394--


Validation issues
Section B. Confidentiality agreement
b1. Recording