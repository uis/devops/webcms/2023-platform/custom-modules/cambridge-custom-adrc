<?php

namespace Drupal\cambridge_custom_adrc\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* @Filter(
*   id = "filter_external",
*   title = @Translation("External link Filter"),
*   description = @Translation("Add class for external icon on external links"),
*   *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
*    weight = 0
* )
*/

class FilterExternal extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Constructs a FilterExternal object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $dom = Html::load($text);
    $anchors = $dom->getElementsByTagName('a');
    foreach ($anchors as $anchor) {

      $current_domain = parse_url($this->request->getSchemeAndHttpHost(), PHP_URL_HOST);
      $current_domain = preg_replace('/^www\./', '', $current_domain);

      $href = parse_url($anchor->getAttribute('href'), PHP_URL_HOST);
      if (!$href) {
        continue;
      }

      $href = preg_replace('/^www\./', '', $href);
      if ($href != $current_domain) {
        $span = $dom->createElement('span');
        $span->setAttribute('class', 'external-link');
        $anchor->appendChild($span);
      }
    }

    return new FilterProcessResult(Html::serialize($dom));
  }
}
